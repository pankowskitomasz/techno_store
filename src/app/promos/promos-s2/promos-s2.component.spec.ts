import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromosS2Component } from './promos-s2.component';

describe('PromosS2Component', () => {
  let component: PromosS2Component;
  let fixture: ComponentFixture<PromosS2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromosS2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromosS2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
