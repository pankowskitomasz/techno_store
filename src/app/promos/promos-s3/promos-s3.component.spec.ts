import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromosS3Component } from './promos-s3.component';

describe('PromosS3Component', () => {
  let component: PromosS3Component;
  let fixture: ComponentFixture<PromosS3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromosS3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromosS3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
