import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromosRoutingModule } from './promos-routing.module';
import { PromosComponent } from './promos/promos.component';
import { PromosS1Component } from './promos-s1/promos-s1.component';
import { PromosS2Component } from './promos-s2/promos-s2.component';
import { PromosS3Component } from './promos-s3/promos-s3.component';


@NgModule({
  declarations: [
    PromosComponent,
    PromosS1Component,
    PromosS2Component,
    PromosS3Component
  ],
  imports: [
    CommonModule,
    PromosRoutingModule
  ]
})
export class PromosModule { }
