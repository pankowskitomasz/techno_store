import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PromosS1Component } from './promos-s1.component';

describe('PromosS1Component', () => {
  let component: PromosS1Component;
  let fixture: ComponentFixture<PromosS1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PromosS1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PromosS1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
