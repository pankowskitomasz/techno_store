import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PromosComponent } from './promos/promos.component';

const routes: Routes = [
  {
    path:"promos",
    component: PromosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromosRoutingModule { }
